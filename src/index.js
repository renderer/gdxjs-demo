import "./index.css";
import resizeCanvas from "gdxjs/lib/resizeCanvas";
import createWhiteTex from "gl-white-texture";
import createBatch from "gdxjs/lib/createBatch";
import createCamera from "gdxjs/lib/orthoCamera";
import InputHandler from "gdxjs/lib/InputHandler";
import loadTexture from "gdxjs/lib/loadTexture";
import loadAtlas from "gdxjs/lib/loadAtlas";
import TextureRegion from "gdxjs/lib/TextureRegion";
import createAnimation, { PlayMode } from "gdxjs/lib/createAnimation";
import Vector2 from "gdxjs/lib/vector2";

const init = async () => {
  const info = document.getElementById("info");
  const canvas = document.getElementById("main");
  const [width, height] = resizeCanvas(canvas);

  const worldWidth = 60;
  const worldHeight = 40;

  const SPEED = 14;

  const gl = canvas.getContext("webgl");

  const cam = createCamera(worldWidth, worldHeight, width, height);
  const batch = createBatch(gl);

  const background = await loadTexture(gl, "./background.png");
  const spriteSheet = await loadTexture(gl, "./spriteSheet.png");
  const effects = await loadAtlas(gl, "./effect.atlas");

  const frames = TextureRegion.splitTexture(spriteSheet, 2, 4);

  const animation = createAnimation(
    0.1,
    [2, 3, 4, 5, 6, 7].map(i => frames[i])
  );

  const explosion = createAnimation(0.1, effects.findRegions("explosion"));

  const pos = new Vector2(0, 0);
  const target = new Vector2(0, 0);

  const inputHandler = new InputHandler(canvas);
  const worldPosition = [];
  const screenPosition = [];
  inputHandler.addEventListener("touchStart", (x, y) => {
    screenPosition[0] = x;
    screenPosition[1] = y;
    cam.unprojectVector2(worldPosition, screenPosition);
    target.set(worldPosition[0], worldPosition[1]);
    console.log(target);
  });

  const whiteTex = createWhiteTex(gl);

  gl.clearColor(0, 0, 0, 1);

  let rotation = 0;

  const tmp = new Vector2();
  let stateTime = 0;
  const update = delta => {
    stateTime += delta;
    rotation += delta * 2 * Math.PI;
    gl.clear(gl.COLOR_BUFFER_BIT);

    tmp
      .setVector(target)
      .subVector(pos)
      .nor()
      .scale(delta * SPEED);
    if (tmp.len2() >= pos.distanceSqr(target)) {
      pos.setVector(target);
    } else {
      pos.addVector(tmp);
    }

    batch.setProjection(cam.combined);
    batch.begin();
    batch.draw(background, 0, 0, worldWidth, worldHeight);
    batch.draw(whiteTex, pos.x - 5, pos.y - 5, 10, 10, rotation);
    animation.getKeyFrame(stateTime, PlayMode.LOOP).draw(batch, 10, 30, 10, 10);
    explosion.getKeyFrame(stateTime, PlayMode.LOOP).draw(batch, 10, 30, 10, 10);
    batch.end();
  };

  let lastUpdate = Date.now();
  let fps = 0;
  (function loop() {
    const delta = Date.now() - lastUpdate;
    lastUpdate = Date.now();
    fps = Math.floor(1000 / delta);
    update(delta / 1000);
    requestAnimationFrame(loop);
  })();

  setInterval(() => (info.innerHTML = `FPS: ${fps}`), 1000);
};

init();
